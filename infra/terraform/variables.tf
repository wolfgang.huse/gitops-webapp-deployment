variable "nutanix_endpoint" {
  description = "Nutanix PC endpoint"
  type = string
  default = "172.23.0.7"
}

variable "nutanix_cluster" {
  description = "Nutanix PE Cluster"
  type = string
  default = "NTNX-DEMO01"
}

variable "nutanix_network" {
  description = "Nutanix Subnet"
  type = string
  default = "NTNX-DEMO01_IPAM"
}

variable "nutanix_user" {
    description = "Nutanix PC user"
    type = string 
}
variable "nutanix_password" {
    description = "Nutanix PC password"
    type = string
    sensitive = true 
}

variable "nutanix_pe_user" {
    description = "Nutanix PE user"
    default = "notusedwith2.3"
    type = string 
}

variable "nutanix_pe_password" {
    description = "Nutanix PE  password"
    type = string
    sensitive = true
    default = "notusedwith2.3"
}

# variable "cloudflare_email" {
#   type = string
#   default = "reg-cloudflare@geo6.net"
# }

# variable "cloudflare_api_key" {
#   type = string
#   sensitive = true
# }

# variable "cloudflare_zone_id" {
#   type = string
#   default = "823ee370e6960502490d373e8ad29d26"
# }

variable "argocd_username" {
  type = string
}

variable "argocd_password" {
  type = string
  sensitive = true
}

variable "argocd_server_addr" {
  type = string
  default = "172.23.131.144:443"
}



variable "name" {
  type = string
}

variable "admin" {
  type = string
  default = "none"
}

variable "addons" {
  type = bool
  default = true
}

variable "oidc" {
  type = bool
  default = false
}

variable "argocd" {
  type = bool
  default = false
}
