resource "helm_release" "metallb" {
  count             = var.addons ? 1 : 0
  name              = "metallb"
  namespace         = "metallb-system"
  create_namespace  = true

  repository = "https://metallb.github.io/metallb"
  chart = "metallb"

  values = [
    "${file("helm-values/metallb-values.yaml")}"
  ]
}

resource "helm_release" "cert-manager" {
  count             = var.addons ? 1 : 0
  name              = "cert-manager"
  namespace         = "cert-manager"
  create_namespace  = true

  repository = "https://charts.jetstack.io"
  chart = "cert-manager"

  set {
    name = "installCRDs"
    value = "true"
  }
} 

resource "helm_release" "ingress-nginx" {
  count             = var.addons ? 1 : 0
  name              = "ingress-nginx"
  namespace         = "nginx-system"
  create_namespace  = true

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"

  set {
    name  = "service.type"
    value = "ClusterIP"
  }

}

