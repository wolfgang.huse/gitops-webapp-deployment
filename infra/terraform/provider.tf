terraform {
  required_providers {
    nutanix = {
      source = "nutanix/nutanix"
      version = "1.2.1"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.3.2"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.23.0"
    }

      argocd = {
      source = "oboukili/argocd"
      version = "2.1.0"
    }
  
    helm = {
      source = "hashicorp/helm"
      version = "2.2.0"
    }

    local = {
      source = "hashicorp/local"
      version = "2.1.0"
    }
  }
}

provider "nutanix" {
  username     = var.nutanix_user
  password     = var.nutanix_password
  endpoint     = var.nutanix_endpoint
  insecure     = true
  port         = 9440
  wait_timeout = 30
}

# provider "cloudflare" { 
#   email   = var.cloudflare_email
#   api_key = var.cloudflare_api_key
# }

provider "kubernetes" {
    host = module.karbon.kubeconfig.cluster_url
    insecure = true
    token = module.karbon.kubeconfig.access_token
}

provider "helm" {
  kubernetes {
    host = module.karbon.kubeconfig.cluster_url
    insecure = true
    token = module.karbon.kubeconfig.access_token
  }
    
}

provider "argocd" {
  server_addr = var.argocd_server_addr
  username  = var.argocd_username
  password = var.argocd_password
  insecure = true
}