# -*- mode: yaml; -*-
# vim: syntax=yaml
#
apiVersion: v1
kind: Config
clusters:
- name: ${name}
  cluster:
    server: ${cluster_url}
    certificate-authority-data: ${certificate-authority-data}
users:
- name: admin-user-${name}
  user:
    token: ${token}
contexts:
- context:
    cluster: ${name}
    user: admin-user-${name}
  name: ${name}-context
current-context: ${name}-context
