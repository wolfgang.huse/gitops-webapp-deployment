resource "argocd_cluster" "client_argocd" {
  count             = var.argocd ? 1 : 0

  server     = module.karbon.kubeconfig.cluster_url
  name       = var.name
  config {
    bearer_token = data.kubernetes_secret.serviceaccount-token.data["token"]
    #bearer_token = module.karbon.kubeconfig.access_token
    tls_client_config {
      ca_data = data.kubernetes_secret.serviceaccount-token.data["ca.crt"]
      insecure = false
    }
  }
}