# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "2.23.0"
  constraints = "2.23.0"
  hashes = [
    "h1:aAk3lJf1KBDVHQZZ0IX8M802axkXocyi0zxW/gvNFjs=",
    "zh:11008f1455ec8f7d3637d60e3b90d0e367b816289d6d99e61370e7fd2f906ba3",
    "zh:1d84ee317977ea925d5249cd33ce78258fd0c0e34892e0658ce2b9ea6172c006",
    "zh:207e2b21b8a9b48e1dbaf088da18a9ce197f54759fb5a6d73869e2089981b351",
    "zh:25d13eba622c684ad3fc79ac2581b7dae25fa563b26004291150212f551b82d7",
    "zh:268a0d143825630665063d99d5ca053298be3fdf01e553e0bff098dc971ede4b",
    "zh:2c3e231a0b4091801b3fbec8b49cc43248e63222bd18ed87eecdd0c91ad37be7",
    "zh:49452aa77543b7a623080b072c2fc5dbab21ea13304230a91547a536749a8201",
    "zh:6550cbdea723eec29530ef9430fcc9c6ceb2f9dd76e7e44dece0755dbc0cfed6",
    "zh:7dac5f84a2bb20d1ef3c5dc53c622c4b7cfef51eb93f90379184b14accd1feff",
    "zh:813dd50b61bb3fce7594359a2c40cd3f7971d0c33eeda1f8c7c29775e35366ce",
    "zh:982a5629e8362a5ed7a21bb98339af918215ab8dfe6e7221109d8af87cc44006",
    "zh:a0446f88d88ddee7bf058638f59238bde9a0d3f447f78cafe6764265513b931c",
    "zh:af6e9acb939fec7151ba1be0c43ccbb7d8fb122d6fb7f4c85a1872a2f8b88edc",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.2.0"
  constraints = "2.2.0"
  hashes = [
    "h1:liBgOoOXhA2A1DbL0oaifyNnoGOyHxMG4+xD1Kl58XA=",
    "zh:01341dd1e9cc7e7f6999e11e7473bcdca2dd72dd27f91beed1f4fb599a15dfba",
    "zh:20e86c9eccd3a81ef5ac243af31b61fc4d2d679437384bd0870e92fa1b3ed6c9",
    "zh:22a71127c5dbea4f62edb5bcf00b5c163de04aa19d45a7a1f621f973ffd09d20",
    "zh:28ab7c84a5f8ed82fc520668db93d650571ddf59d98845cb18a1fa1a7888efc0",
    "zh:3985a30929ad8fdc6b94f0e1cbd62a63db75ee961b8ba7db1cf4bfd29e8009ff",
    "zh:477d92e26ba0c906087a5dd827ac3917dad7d5af770ee0ab4b08d0f273150586",
    "zh:750928ec5ef54b2090bd6a6d8a19630a8712bbbccc0429251e88ccd361c1d3c0",
    "zh:a615841fd90094bddc1269127e501fa60453c441b9548ff73752fe14efc38ed0",
    "zh:e762aca7883374fa255efba50f5bdf791fece7d61e3920e593fb1a2cbb598981",
    "zh:f76f372ead52948ca53610b371cb80c80ebcf058ef0a5c0ce9f0ce38dcc9a8eb",
    "zh:fa36fe93ed977f4478cc6547ec3c45c28e56f10632e85446b0c3d71449f8c4bb",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.3.2"
  constraints = "2.3.2"
  hashes = [
    "h1:qxMfKMH8V4m+6oWbH8l/6LvvBJx5NEbJoXruL2OgNsI=",
    "zh:10f71c170be13538374a4b9553fcb3d98a6036bcd1ca5901877773116c3f828e",
    "zh:11d2230e531b7480317e988207a73cb67b332f225b0892304983b19b6014ebe0",
    "zh:3317387a9a6cc27fd7536b8f3cad4b8a9285e9461f125c5a15d192cef3281856",
    "zh:458a9858362900fbe97e00432ae8a5bef212a4dacf97a57ede7534c164730da4",
    "zh:50ea297007d9fe53e5411577f87a4b13f3877ce732089b42f938430e6aadff0d",
    "zh:56705c959e4cbea3b115782d04c62c68ac75128c5c44ee7aa4043df253ffbfe3",
    "zh:7eb3722f7f036e224824470c3e0d941f1f268fcd5fa2f8203e0eee425d0e1484",
    "zh:9f408a6df4d74089e6ce18f9206b06b8107ddb57e2bc9b958a6b7dc352c62980",
    "zh:aadd25ccc3021040808feb2645779962f638766eb583f586806e59f24dde81bb",
    "zh:b101c3456e4309b09aab129b0118561178c92cb4be5d96dec553189c3084dca1",
    "zh:ec08478573b4953764099fbfd670fae81dc24b60e467fb3b023e6fab50b70a9e",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version     = "2.1.0"
  constraints = "2.1.0"
  hashes = [
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/nutanix/nutanix" {
  version     = "1.2.1"
  constraints = "1.2.1"
  hashes = [
    "h1:Z68k6pwBns80P2x2L4EEwHX2YiWlPQkbFm1tVpTWQoc=",
    "zh:064a55ba20c0e30679c6bd27fdd5252d2794683e60216976be9980ab2f32bdb5",
    "zh:08ed59cda5126679560fffb6b8d0ea58c2bb2198d967dbe699fdb2bdbe38f738",
    "zh:1e4c7fe4c2f7a2434564f6db71732a0c69e30d3ca2a52bd1c3d326937edb8544",
    "zh:5f7b4e0cb25a34f01d00abd24cd859ab099a8838925c425d5d0b80e976b06366",
    "zh:657322f86fe0bb01cb76745da9e849c17dc393e3e321483a7b85573eed6ae5d1",
    "zh:6e47da10ec784cf5912a220f4097b15acd73a38b149002891ed3253a3e20524b",
    "zh:79dcf36d7cd932bd1ffef98f6a6cb29a40f8580df66305183a785ff9868933cd",
    "zh:7ebd04fc7908d9f5c507e30146e2fd745338d3e8e2d8e3d3844a574153e51662",
    "zh:939b7010a459f244164189e31cb000c9e4f1be1e82aab6901ca36646e182fb9a",
    "zh:9923910e56f85b705878aca18036030b156196310ac06fedaf60152f88a59669",
    "zh:c8e760fcef0813aea677ec32e1221e18094069272c4513ce1b6bedf90fac67d5",
    "zh:ce78924621b0bae7fbe910e5072b0d7f8e2018231df6a6748d49866edd49a98d",
  ]
}

provider "registry.terraform.io/oboukili/argocd" {
  version     = "2.1.0"
  constraints = "2.1.0"
  hashes = [
    "h1:SbUxgB3DQGJDHQC0o6vpXYaYOXVEFRWgmFM8nFp1qzU=",
    "zh:088634e5aae5b2e924547d48a2a6f9dcff75b15404eadec2be2d00cd0c5e8d34",
    "zh:12ee52c2fbead0202ee217c5eb1d7bba1b5f97d049677ca54b0f74a7e57915b9",
    "zh:2037960950e1f6759c5ff4326edd943db642a37b8ca7d7c4fe6a9d33acb31268",
    "zh:23dd2859ce96dc095f2c9f83783dfe39befdc42d88d32429ae947fcce0da0061",
    "zh:30697cb25b1468d7182b18549fc8b26585ff141200c267983f5248fc0f3ef850",
    "zh:763f0dad55858921a458d6c0fa41dba43e61334ff21fcdc2bad4e1a5a623883e",
    "zh:8d71ea2f26b7fd517a0966b8b6efac32cd94ca2369452f000db437074d456455",
  ]
}
