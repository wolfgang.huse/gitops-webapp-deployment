output "uuid" {
    description = "uuid of the cluster"
    value = nutanix_karbon_cluster.karbon.id
  
}



output "kubeconfig" {
    description = "kubeconfig of the cluster"
    value = data.nutanix_karbon_cluster_kubeconfig.karbonconfig
}

output "worker_ip" {
    value = nutanix_karbon_cluster.karbon.worker_node_pool[0].nodes[0].ipv4_address
}