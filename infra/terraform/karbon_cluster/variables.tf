variable "name" {
    description = "name"
    type = string
  
}

variable "node_os_version" {
    description = "node_os_version"
    type = string
    default = "ntnx-1.1"
  
}

variable "kubernetes_version" {
    description = "kubernetes_version"
    type = string
    default = "1.20.9-0"
  
}

variable "karbon_network" {
    description = "Karbon network"
    type = string
}

variable "karbon_cluster" {
    description = "Karbon cluster"
    type = string
}

variable "pod_ipv4_cidr" {
    description = ""
    type = string
    default = "172.20.0.0/16"
}

variable "service_ipv4_cidr" {
    description = "service_ipv4_cidr"
    type = string
    default = "172.19.0.0/16"
  
}

variable "node_cidr_mask_size" {
    description = "node_cidr_mask_size"
    type  = number
    default = 24
}
variable "nutanix_pe_user" {
    description = "PE user"
    type = string
}

variable "nutanix_pe_password" {
    description = "Pe Password"
    type = string
    sensitive = true 
}