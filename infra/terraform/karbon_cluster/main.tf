
terraform {
  required_providers {
    nutanix = {
      source = "nutanix/nutanix"
      version = "1.2.1"
    }
  }
}

data "nutanix_subnet" "karbon_subnet" {
  subnet_name = var.karbon_network
}

data "nutanix_cluster" "karbon_cluster" {
  name = var.karbon_cluster
}

resource "nutanix_karbon_cluster" "karbon" {
    name = var.name
    version = var.kubernetes_version

    etcd_node_pool {
      node_os_version = var.node_os_version
      num_instances = 1
      ahv_config {
        cpu = 4
        memory_mib = 8192
        disk_mib = 40960
        prism_element_cluster_uuid = data.nutanix_cluster.karbon_cluster.id
        network_uuid = data.nutanix_subnet.karbon_subnet.id
      }
    }

    master_node_pool {
        node_os_version = var.node_os_version
        num_instances = 1
        ahv_config {
            cpu = 2
            memory_mib = 4096
            disk_mib = 122880
            prism_element_cluster_uuid = data.nutanix_cluster.karbon_cluster.id
            network_uuid = data.nutanix_subnet.karbon_subnet.id
      }
    }

    worker_node_pool {
        node_os_version = var.node_os_version
        num_instances = 2
        ahv_config {
            cpu = 8
            memory_mib = 8192
            disk_mib = 122880
            prism_element_cluster_uuid = data.nutanix_cluster.karbon_cluster.id
            network_uuid = data.nutanix_subnet.karbon_subnet.id
      }
    }

    cni_config {
      node_cidr_mask_size = var.node_cidr_mask_size
      pod_ipv4_cidr = var.pod_ipv4_cidr
      service_ipv4_cidr = var.service_ipv4_cidr
      # flannel_config {
      # }
      calico_config {
        ip_pool_config {
          cidr = var.pod_ipv4_cidr
        } 

      }
    }

    storage_class_config {
        name = "nutanix-volume"
        reclaim_policy = "Delete"
    
        volumes_config {
            file_system = "xfs"
            flash_mode = false
            password = var.nutanix_pe_password
            prism_element_cluster_uuid = data.nutanix_cluster.karbon_cluster.id
            storage_container = "k8s"
            username = var.nutanix_pe_user
        }
    }
}

data "nutanix_karbon_cluster_kubeconfig" "karbonconfig" {
  karbon_cluster_id = resource.nutanix_karbon_cluster.karbon.id
}


