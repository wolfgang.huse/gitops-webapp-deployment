module "karbon" {
  source  = "./karbon_cluster"

  name = "${var.name}"

  karbon_cluster = "${var.nutanix_cluster}"
  karbon_network = var.nutanix_network
  nutanix_pe_user = var.nutanix_pe_user
  nutanix_pe_password = var.nutanix_pe_password
}

resource "local_file" "kubeconfig" {
  content     = templatefile("templates/kubeconfig.tpl",{
    name = var.name,
    #certificate-authority-data = module.karbon.kubeconfig.cluster_ca_certificate,
    #token = module.karbon.kubeconfig.access_token,
    certificate-authority-data = base64encode(data.kubernetes_secret.serviceaccount-token.data["ca.crt"])
    token = data.kubernetes_secret.serviceaccount-token.data["token"],
    cluster_url = module.karbon.kubeconfig.cluster_url
  })
  filename = "${var.name}.kubeconfig"
  file_permission = "0600"
}
