data "kubernetes_secret" "serviceaccount-token" {
  metadata {
    name      = "${kubernetes_service_account.serviceaccount-sa.default_secret_name}"
    namespace = "${kubernetes_service_account.serviceaccount-sa.metadata.0.namespace}"
  }
}

resource "kubernetes_service_account" "serviceaccount-sa" {
  metadata {
    generate_name = "serviceaccount-sa"
    namespace = "kube-system"
    
  }
}

resource "kubernetes_cluster_role_binding" "serviceaccount-crb" {
  metadata {
    name = "serviceaccount-crb"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "${kubernetes_service_account.serviceaccount-sa.metadata.0.name}"
    namespace = "${kubernetes_service_account.serviceaccount-sa.metadata.0.namespace}"
  }
}