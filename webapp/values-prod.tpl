replicaCount: 2

image:
  repository: registry.gitlab.com/wolfgang.huse/gitops-webapp
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: "${CI_COMMIT_SHA}"

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: true
  className: "nginx"
  hosts:
    - host: webapp.prod.ntnx.test
      paths:
        - path: /
          pathType: Prefix

dbuser: "${DBUSER}"
dbpass: "${DBPASS}"
dbhost: "${DBHOST}"
dbport: "${DBPORT}"
dbname: "${DBNAME}"
endpoint: "${ENDPOINT}"
accesskey: "${ACCESSKEY}"
secretkey: "${SECRETKEY}"
bucket: "${BUCKET}"
